#!/bin/bash

################################################################################
# Tcl/Tk repository scritps configuration
# ASSUMPTION: The "isw-git-tools" was added as git submodule.
################################################################################

################################################################################
# BEGIN OF CONFIGURATION SECTION
# CONFIGURE THE TEMPLATE HERE
################################################################################

# The path to the "isw-git-tools" submodule directory relative from the location
# of this config file, e.g.:
# "../submodules/isw-git-tools"
modulePath="../submodules/isw-git-tools"

# Tcl/Tk package name, e.g.: "icemops"
# For a Tcl/Tk package name ONLY letters (upper and lower case, but no special
# characters like 'Ö', 'Ü', ...) and the characters '-' and '_' are allowed.
# shellcheck disable=SC2034
packageName="tcllogger"

# The path to the directory which contains the Tcl/Tk package directory (e.g.:
# "icemops0.0.1"), relative from the location of this config file.
# e.g.: ".."
# shellcheck disable=SC2034
packageDir=".."

# The git branch which is used for productive code (not for development) normally
# this is the master branch.
# shellcheck disable=SC2034
productionBranch="master"

################################################################################
# END OF CONFIGURATION SECTION
# DO NOT CHANGE CODE BELOW THIS LINE!
################################################################################

# Get absolute path to script directory
scriptDir=$(cd "$(dirname $0)" && pwd)
scriptDir=$(readlink -f "$scriptDir")

# Resolve realtive paths.
modulePath=$(readlink -f "$scriptDir/$modulePath")
packageDir=$(readlink -f "$scriptDir/$packageDir")

# Resolve path to repo scripts for a tcl/tk repository.
# shellcheck disable=SC2034
scriptPath=$(readlink -f "$modulePath/repo-scripts/tcl-tk/scripts")
