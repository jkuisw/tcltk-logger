# package: tcllogger
# description:
#   This script adds the library paths to the submodules to the tcl package
#   path. It should be sourced by every package script file and will be executed
#   only once. The first package script file which is executed, executes this
#   script.

# Register the package, must be done for every package script file.
source [file join [file dirname [info script]] "packageVersion.tcl"]
package provide $pkgName $pkgVersion

# Initialization that must be done only once.
global isPkgTclloggerInitialized
if {[info exists isPkgTclloggerInitialized] && $isPkgTclloggerInitialized} {
  # package tcllogger is already initialized.
  return
}

# Set library paths.
global auto_path

# Paths to package directories relative from this script.
set paths [list \
  [file join ".." "submodules" "tcltk-utils"] \
]

# Add the package paths to the global auto_path variable.
foreach path $paths {
  set libpath [file normalize [file join [file dirname [info script]] $path]]
  if {[lsearch $auto_path $libpath] == -1} {lappend auto_path "$libpath"}
}

# Package icemops is initialized.
set isPkgTclloggerInitialized true
