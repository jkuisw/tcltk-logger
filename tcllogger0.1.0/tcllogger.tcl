# package: tcllogger
# description:
#   This package implements a simple logger. The tcllogger can be configured with
#   the "::tcllogger::configure" procedure. For possible settings, see the settings
#   array below, or call the function "::loggger::printConfigs". To log a
#   message, just use the "::tcllogger::log" command.

# Initialize package.
source [file join [file dirname [info script]] "init-package.tcl"]

# Dependencies
package require Tcl 8.4
package require utils

# Define the namespace.
namespace eval ::tcllogger {
  # Export commands.
  namespace export \
    configure \
    printConfigs \
    log \
    logError \
    getParameterValue \
    getPossibleParameterValues \
    getParameterDefault \
    matchParameterValue \
    isValidValue

  # Define namespace variables.
  # tcllogger possible log level definitions. The following list defines the
  # possible log levels. A log level list entry consists of the level (integer)
  # and the log level name. The level defines the log order. For example if the
  # log level "info" is chosen, than all log messages with a log level equal or
  # lower than 2 (which is the log level for info) will be printed, all above
  # not.
  # Log level list entry description ([0] | [1]):
  # log level | log level name
  variable possibleLogLevels [list \
    { 0 "error" } \
    { 1 "warn" } \
    { 2 "info" } \
    { 3 "debug" } \
    { 4 "trace" } \
  ]

  # tcllogger possible settings list. The following array holds all possible
  # tcllogger settings. The settings array key is the parameter(setting) name
  # and the corresponding value is a list which has three entries:
  #   1) The current value of the parameter(setting).
  #   2) A list of possible parameter(setting) values. Each possible value can
  #      have alias values and therefore would be a list of alias values. For
  #      example: Each possible log level value has two alises, the level itself
  #      or the log level name.
  #   3) The default value of the parameter(setting).
  # Description of the parameters(settings):
  #   *) logger: Choose a logger which should be used for printing the messages.
  #      Currently the following are supported: "icem", "standard"
  #   *) logLevel: Defines the log level, see the possibleLogLevels variable
  #      above for further information.
  #   *) noNewLine: Defines if there should be a new line after printing the
  #      log message. "yes" means no new line will be printed after the message.
  #      Possible values: "yes", "no"
  variable settings
  array set settings [list \
    "logger" [list "standard" { "icem" "standard" } "standard"] \
    "logLevel" [list { 2 "info" } $possibleLogLevels { 2 "info" }] \
    "noNewLine" [list "no" { "yes" "no" } "no"] \
  ]
}

# ------------------------------------------------------------------------------
# With this function the tcllogger can be configured.
#
# Parameters:
#   parameter - The tcllogger parameter(setting) which should be configured.
#   value - The value for the setting.
proc ::tcllogger::configure { parameter value } {
  variable settings

  # Check if parameter exists.
  if {[array names settings -exact $parameter] == ""} {
    logError "::tcllogger::configure" "The parameter \"$parameter\" does not exist!"
  }

  # Check if value is valid for the parameter.
  set possibleParameterValues [getPossibleParameterValues $parameter]
  if {[isValidValue $value $possibleParameterValues] == false} {
    logError "::tcllogger::configure" [format "%s\n%s" \
      "The value \"$value\" is not valid for the parameter \"$parameter\"!" \
      "Valid values are: [getPossibleParameterValues $parameter]"]
  }

  set paramData [lindex [array get settings $parameter] 1]
  #puts "paramData: $paramData"
  lset paramData 0 [matchParameterValue $value $possibleParameterValues]
  #puts "paramData: $paramData"
  array set settings [list $parameter $paramData]
  #puts "setting: [list $parameter $paramData]"
}

# ------------------------------------------------------------------------------
# Retrieve the current value of a tcllogger parameter(setting).
# Assumption: The parameter(setting) exists.
#
# Parameters:
#   parameter - The tcllogger parameter(setting) who`s value should be returned.
#
# Returns: The parameter(setting) value.
proc ::tcllogger::getParameterValue { parameter } {
  variable settings
  set paramData [lindex [array get settings $parameter] 1]
  return [lindex $paramData 0]
}

# ------------------------------------------------------------------------------
# Retrieve a list of possible tcllogger parameter(setting) values.
#
# Parameters:
#   parameter - The tcllogger parameter(setting).
#
# Returns: A list of possible tcllogger parameter(setting) values.
proc ::tcllogger::getPossibleParameterValues { parameter } {
  variable settings
  set paramData [lindex [array get settings $parameter] 1]
  return [lindex $paramData 1]
}

# ------------------------------------------------------------------------------
# Retrieve the default values of a tcllogger parameter(setting).
#
# Parameters:
#   parameter - The tcllogger parameter(setting).
#
# Returns: The default value of a tcllogger parameter(setting).
proc ::tcllogger::getParameterDefault { parameter } {
  variable settings
  set paramData [lindex [array get settings $parameter] 1]
  return [lindex $paramData 2]
}

# ------------------------------------------------------------------------------
# Match the given value with possible tcllogger parameter(setting) values and
# return the match. For example the value could be a single value alias, than
# the function will return all possible tcllogger parameter(setting) alias values
# as a list.
# Assumption: The value is already valid (checked with ::tcllogger::isValidValue).
#
# Parameters:
#   value - The possible tcllogger parameter value alias.
#   possibleValuesList - The list of possible tcllogger parameter values for a
#     specific tcllogger parameter the value belongs to.
#
# Returns: The value match.
proc ::tcllogger::matchParameterValue { value possibleValuesList } {
  # Iterate through list of possible values.
  foreach possVal $possibleValuesList {
    # Check if possible value has aliases.
    if {[llength $possVal] <= 1} {
      # Possible value has no alias, therefore check if found value.
      if {$possVal == $value} { return $possVal; }
    } else {
      # Possible value has aliases, therefore check all aliases.
      foreach possValAlias $possVal {
        if {$possValAlias == $value} { return $possVal; }
      }
    }
  }
}

# ------------------------------------------------------------------------------
# Checks if the given value is a possible tcllogger parameter(setting) value alias.
#
# Parameters:
#   parameter - [default ] description
#
# Returns: "true" if the given value is a possible tcllogger parameter(setting)
#   value alias, "false" otherwise.
proc ::tcllogger::isValidValue { value possibleValuesList } {
  # Iterate through list of possible values.
  foreach possVal $possibleValuesList {
    # Check if possible value has aliases.
    if {[llength $possVal] <= 1} {
      # Possible value has no alias, therefore check if is value.
      if {$possVal == $value} { return true; }
    } else {
      # Possible value has aliases, therefore check all against value.
      foreach possValAlias $possVal {
        if {$possValAlias == $value} { return true; }
      }
    }
  }

  return false;
}

# ------------------------------------------------------------------------------
# Prints all tcllogger settings, their current value, all possible values and
# their default value.
proc ::tcllogger::printConfigs {} {
  variable settings

  if {[lindex [getParameterValue "logLevel"] 0] < 2} {
    logError "::tcllogger::printConfigs" [format \
      "Log level has to be at least 2 (info) to print configs! Current is: %s" \
      [getParameterValue "logLevel"]
    ]
  }

  set noNewLineBackup [getParameterValue "noNewLine"]

  # Deactivate new line.
  configure "noNewLine" "yes"

  # Print all parameters.
  log 2 "--------------------------------------------------------------------\n"
  log 2 "tcllogger possible configuration parameters:\n"
  foreach {key value} [array get settings] {
    log 2 [::utils::indentText "parameter \"$key\":\n"]

    if {$key == "noNewLine"} {
      log 2 [::utils::indentText "value: $noNewLineBackup\n" 6]
    } else {
      log 2 [::utils::indentText "value: [lindex $value 0]\n" 6]
    }

    log 2 [::utils::indentText "possible values: [lindex $value 1]\n" 6]
    log 2 [::utils::indentText "default value: [lindex $value 2]\n" 6]
  }
  log 2 "--------------------------------------------------------------------\n"

  # Restore noNewLine setting.
  configure "noNewLine" $noNewLineBackup
}

# ------------------------------------------------------------------------------
# Prints the message if the given log level is lower or equal to the configured
# log level.
#
# Parameters:
#   level - [default "info"] The log level.
#   message - The message which should be printed.
proc ::tcllogger::log { {level "info"} message } {
  # Check if level is a valid log level.
  set possibleLogLevels [getPossibleParameterValues "logLevel"]
  if {[isValidValue $level $possibleLogLevels] == false} {
    logError "::tcllogger::log" [format \
      "The log level \"%s\" is not valid!\nPossible log levels are: %s" \
      $level $possibleLogLevels
    ]
  }

  set messageLogLevel [matchParameterValue $level $possibleLogLevels]
  set configuredLogLevel [getParameterValue "logLevel"]

  if {[lindex $configuredLogLevel 0] >= [lindex $messageLogLevel 0]} {
    if {[lindex $messageLogLevel 1] == "error"} {
      errorLog $message
    } else {
      switch -exact -- [getParameterValue "logger"] {
        "standard" { standardLog $message }
        "icem" { icemLog $message }
        default {
          logError "::tcllogger::log" \
            "The logger \"[getParameterValue "logger"]\" is not supported!"
        }
      }
    }
  }
}

# ------------------------------------------------------------------------------
# Logs an error message.
#
# Parameters:
#   procedure - [default ""] The procedure in which the error occured.
#   message - The message which should be printed.
proc ::tcllogger::logError { args } {
  set argcnt [llength $args]

  if {$argcnt < 1 || $argcnt > 2} {
    error "wrong # args: should be \"::tcllogger::logError ?procedure? message\""
  } elseif {$argcnt == 1} {
    set procedure ""
    set message [lindex $args 0]
  } else {
    set procedure [lindex $args 0]
    set message [lindex $args 1]
  }

  ::tcllogger::errorLog $message $procedure
}

# ------------------------------------------------------------------------------
# Logs an error message.
#
# Parameters:
#   message - The message which should be printed.
#   procedure - [default ""] The procedure in which the error occured.
proc ::tcllogger::errorLog { message {procedure ""} } {
  if {$procedure == ""} {
    error "\n$message\n"
  } else {
    error [format "\n%s\n$message\n" "The procedure \"$procedure\" failed!" ]
  }
}

# ------------------------------------------------------------------------------
# Logs a message with the ICEM CFD printing command.
#
# Parameters:
#   message - The message which should be printed.
proc ::tcllogger::icemLog { message } {
  if {[getParameterValue "noNewLine"] == "yes"} {
    ic_mess "$message"
  } else {
    ic_mess "$message\n"
  }
}

# ------------------------------------------------------------------------------
# Logs the message with the standard Tcl printing command.
#
# Parameters:
#   message - The message which should be printed.
proc ::tcllogger::standardLog { message } {
  if {[getParameterValue "noNewLine"] == "yes"} {
    puts -nonewline "$message"
  } else {
    puts "$message"
  }
}

# ------------------------------------------------------------------------------
# TODO: description
#
# Parameters:
#   parameter - [default ] description
#
# Returns:
